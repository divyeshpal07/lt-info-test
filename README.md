Run Docker in local machine

Open Terminal
- Clone Project
- navigate to project folder
- run Docker in local machine
- open Makefile
- run make DockerBuild -> to build docker image of the project
- run make DockerRun -> to run docker image on port=8080